import java.util.Random;
class Die{
	private int faceValue;
	private Random random;
	
	public Die(){
		random = new Random();
		this.faceValue = 1;
		roll();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		random = new Random();
		this.faceValue = random.nextInt(5)+1;
	}
	
	public String toString(){
		return "" + this.faceValue;
	}
}