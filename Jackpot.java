class Jackpot{
	public static void main (String[] args) {
		System.out.println("Welcome to the Jackpot Game!");
		Board board1 = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver == false){
			System.out.println(board1);
			boolean check = board1.playATurn();
			if(check == true){
				gameOver = true;
			}else{
				numOfTilesClosed += 1;
			}
		}
		if(numOfTilesClosed >= 7){
			System.out.println("Congratulations! You hit the jackpot!");
		}else{
			System.out.println("You lost... Better luck next time!");
		}
	}
}