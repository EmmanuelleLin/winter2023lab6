class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String board = "";
		for(int i = 0; i < tiles.length; i++){
			if(this.tiles[i] == false){
				board += i+1 + " ";
			}else{
				board += "X ";
			}
		}
		return board;
	}
	
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		
		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		System.out.println(sumOfDice);
		
		if(tiles[sumOfDice-1] == false){
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			tiles[sumOfDice-1] = true;
			return false;
		}
		else if(tiles[this.die1.getFaceValue()-1] == false){
			tiles[this.die1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: " + this.die1.getFaceValue());
			return false;
		}
		else if(tiles[this.die2.getFaceValue()-1] == false){
			tiles[this.die2.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: " + this.die2.getFaceValue());
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut.");
			return true;
		}
		
	}
}